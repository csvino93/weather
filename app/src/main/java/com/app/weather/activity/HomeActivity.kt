package com.app.weather.activity

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.weather.BuildConfig
import com.app.weather.R
import com.app.weather.adapter.WeatherAdapter
import com.app.weather.base.BaseActivity
import com.app.weather.db.Executor.ioThread
import com.app.weather.db.dao.WeatherDao
import com.app.weather.db.entity.Weather
import com.app.weather.db.entity.WeatherDetails
import com.app.weather.extension.hide
import com.app.weather.extension.show
import com.app.weather.model.LatLng
import com.app.weather.model.WeatherModel
import com.app.weather.utils.CommonUtils
import com.app.weather.utils.Vars
import com.app.weather.viewmodel.HomeViewModel
import kotlinx.android.synthetic.main.activity_home.*
import java.lang.Exception

class HomeActivity : BaseActivity() {

    lateinit var homeViewModel: HomeViewModel
    var latLng = LatLng(0.0, 0.0)
    var city = ""
    private lateinit var dao : WeatherDao
    lateinit var weatherAdapter: WeatherAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        // to restrict crash on 8.1.0 custom os devices added below lines
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            window.decorView.importantForAutofill =
                View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS
        }

        attachViewModel()
        initView()
        prepareClickListener()
    }

    private fun initView() {
        toolbar.title = getString(R.string.home)
        setSupportActionBar(toolbar)

        rvWeather.layoutManager = LinearLayoutManager(this@HomeActivity)
        weatherAdapter = WeatherAdapter(this@HomeActivity)
        rvWeather.adapter = weatherAdapter
        tvHint.show()
        clResult.hide()
        dao = weatherDatabase.weatherDao()
    }

    private fun prepareClickListener() {
        btnSubmit.setOnClickListener {
            city = etCity.text.toString().trim()
            if (isValid())
                //showing data from api when internet is available or showing from local db if data is available
                if (CommonUtils.isInternetAvailable(this@HomeActivity)) {
                    //Threading to fetch lat lng from city name
                    ioThread(Runnable {
                        val latLng = getLatLngFromCity(city)
                        runOnUiThread{
                            if (latLng.lat == 0.0 && latLng.lng == 0.0) {
                                etCity.error = getString(R.string.err_enter_valid_city_name)
                            } else {
                                this.latLng = latLng
                                //api call
                                accessCity()
                            }
                        }
                    })
                } else {
                    prepareView()
                }
        }
    }

    private fun isValid(): Boolean {
        var isValid = true
        if (city.isEmpty()) {
            etCity.error = getString(R.string.err_enter_city_name)
            isValid = false
        }
        return isValid
    }

    private fun accessCity() {
        hideKeyboard(this@HomeActivity)
        //showing progress
        llProgressBar.show()
        homeViewModel.accessWeather(
            this@HomeActivity, latLng.lat.toString(), latLng.lng.toString(),
            Vars.FILTER_HOURLY, BuildConfig.APP_KEY
        ).observe(this, Observer {
            //dismissing progress
            llProgressBar.hide()
            if (it != null) {
                //if result is success adding data into db
                addDataToDb(it)
            } else {
                //else checking from local db if its already having any values
                prepareView()
            }
        })
    }

    // formatting data as per db structure
    private fun addDataToDb(it: WeatherModel) {
        val weather = Weather()
        weather.temp = it.current.temp
        weather.dt = it.current.dt
        weather.city = city
        if (it.current.weather.isNotEmpty())
            weather.weather = it.current.weather[0].main

        val weatherDetails = ArrayList<WeatherDetails>()
        for (i in it.daily.indices) {
            var weatherType = ""
            if (it.daily[i].weather.isNotEmpty()) {
                weatherType = it.daily[i].weather[0].main
            }

            val dayDetail = WeatherDetails(
                city,
                it.daily[i].temp.day,
                it.daily[i].dt,
                weatherType
            )
            weatherDetails.add(dayDetail)
        }
        if (weatherDetails.isNotEmpty()) {
            ioThread(Runnable {
                dao.insert(weather)
                dao.removeWeatherDetails(city)
                dao.insert(weatherDetails)
                prepareView()
            })
        }
    }

    @SuppressLint("SetTextI18n")
    private fun prepareView() {
        try {
            ioThread(Runnable {
                val details = dao.getWeather(city)
                runOnUiThread{
                    //updating view here
                    if (details != null) {
                        clResult.show()
                        tvHint.hide()
                        tvWeather.text = details.weather!!.weather
                        tvTemp.text = details.weather!!.temp + getString(R.string.degree_celsius)
                        weatherAdapter.setWeatherDetails(details.days!!)
                    } else {
                        clResult.hide()
                        tvHint.text = getString(R.string.no_data_available)
                        tvHint.show()
                    }
                }
            })
        } catch (e:Exception) {
            e.printStackTrace()
            clResult.hide()
            tvHint.text = getString(R.string.no_data_available)
            tvHint.show()
        }
    }

    //attach view model here
    private fun attachViewModel() {
        homeViewModel = ViewModelProvider(this@HomeActivity).get(HomeViewModel::class.java)
    }
}